# Medical Net - medical history web platform

## Docker service stack management

### Managing individual services

#### Updating a Docker image
```shell script
docker build -f Dockerfile -t registry.gitlab.com/medical-history-platform/web-ui-service:1.0 .
docker push registry.gitlab.com/medical-history-platform/web-ui-service:1.0
```

#### Running standalone service
```shell script
docker run -it --rm -p 80:80 registry.gitlab.com/medical-history-platform/web-ui-service:1.0
```

### Managing full service stack

#### Deploying the stack
```shell script
 docker stack deploy -c stack.yml medical-history-cluster
```

#### Disposing the stack
```shell script
 docker stack rm medical-history-cluster
```