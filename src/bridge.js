import axios from "axios";

const apiRoute = `http://${window.location.hostname}/api/`;
const authApiRoute = `http://${window.location.hostname}/api/auth`;

function getToken() {
    const token = document.cookie
        .split(';')
        .map(cookie => cookie.trim().split('='))
        .filter(cookie => cookie[0] === 'access_token');

    if (token.length)
        return `Bearer ${token[0][1]}`;
    return null;
}

export async function register(data) {
    await axios.post(
        `${authApiRoute}/register`,
        data,
        {
            headers: {'Content-Type': 'application/json'}
        }
    );
}

export async function login(data) {
    const response = await axios.post(
        `${authApiRoute}/login`,
        data,
        {
            headers: {'Content-Type': 'application/json'},
        }
    );
    return response.data;
}

export async function getUserProfile(username) {
    const response = await axios.get(
        `${apiRoute}/users/${username}/profile`,
        {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': getToken(),
            }
        }
    );
    return response.data;
}

export async function getDoctors() {
    const result = await axios.get(`${apiRoute}/doctors`,
        {
            headers: {
                'Authorization': getToken(),
            }
        });
    return result.data;
}

export async function deleteDoctor(id) {
    await axios.delete(`${apiRoute}/doctors/${id}`,
        {
            headers: {
                'Authorization': getToken(),
            }
        });
}

export async function updateDoctorInfo(id, info) {
    await axios.put(`${apiRoute}/doctors/${id}`,
        info,
        {
            headers: {
                'Authorization': getToken(),
            }
        }
    );
}

export async function addDoctor(info) {
    await axios.post(`${apiRoute}/doctors`,
        info,
        {
            headers: {
                'Authorization': getToken(),
            }
        }
    );
}

export async function getRecords(userId) {
    const result = await axios.get(`${apiRoute}/users/${userId}/records`,
        {
            headers: {
                'Authorization': getToken(),
            }
        });
    return result.data;
}

export async function updateAccessGrants(userId, allowedUserId, grantAccess) {
    await axios.put(`${apiRoute}/users/${userId}/records/grants`,
        {
            type: grantAccess ? "grant" : "revoke",
            doctorId: allowedUserId
        },
        {
            headers: {
                'Authorization': getToken(),
            }
        }
    );
}

export async function getAccessGrants(userId) {
    const result = await axios.get(`${apiRoute}/users/${userId}/records/grants`,
        {
            headers: {
                'Authorization': getToken(),
            }
        }
    );
    return result.data.map(grant => grant.doctor_id);
}

export async function getDoctorAccessGrants(doctorUserId) {
    const result = await axios.get(`${apiRoute}/users/${doctorUserId}/grants`,
        {
            headers: {
                'Authorization': getToken(),
            }
        }
    );
    return result.data;
}

export async function registerRecord(userId, record) {
    await axios.post(`${apiRoute}/users/${userId}/records`,
        record,
        {
            headers: {
                'Authorization': getToken(),
            }
        }
    );
}