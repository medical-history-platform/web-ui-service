import * as React from 'react';
import style from '../style.module.scss';
import {getTheme} from "office-ui-fabric-react/lib/Styling";
import {constants} from '../constants';
import {Link} from "office-ui-fabric-react";


const Footer = () => {
    const {palette} = getTheme();

    return (
        <div className={style.footer} style={{backgroundColor: palette.neutralLighter}}>
            <div className={style.centerBox}>
                <div className={style.column}>
                    <h3>{constants.appName}</h3>
                    <p>
                        Copyright &copy; {new Date().getFullYear()} {constants.appName}. All rights reserved.
                    </p>
                </div>
                <div className={style.column}>
                    <h5 style={{marginBottom: 5}}>Contact</h5>
                    <p style={{fontSize: "small"}}>
                        Email: <Link href={'mailto:' + constants.mail}>{constants.mail}</Link>
                        <br/>
                        Tel: <Link href={'tel:' + constants.phone}>{constants.phone}</Link>
                    </p>
                </div>
            </div>
        </div>
    );
};

export default Footer;