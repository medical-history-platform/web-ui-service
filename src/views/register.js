import React, {useState} from 'react';
import '../App.css';
import {Link} from 'react-router-dom';
import * as bridge from "../bridge";
import style from '../style.module.scss';

import {Text, TextField, DefaultButton, PrimaryButton, Stack, getTheme} from 'office-ui-fabric-react';
import {Depths} from '@uifabric/fluent-theme/lib/fluent/FluentDepths';


const RegistrationStates = Object.freeze({
    DATA_FORM: 0,
    REGISTERED: 1,
});

const RegisteredInfoBox = () => {
    const {palette} = getTheme();

    return (
        <>
            <Text variant={'medium'} block>
                <h2>Account created</h2>
                <Text variant={'medium'} block>
                    Your account was successfully created.
                </Text>
                <Link className={style.smallLink} to="/login" style={{color: palette.themePrimary}}>Go to login
                    page</Link>
                <br/>
            </Text>
        </>
    );
};

export function RegisterBox() {
    const {palette} = getTheme();
    const [err, setErr] = useState({username: "", name: "", email: "", password: ""});
    const [values, setValues] = useState({username: "", name: "", email: "", password: ""});
    const [registrationState, setRegistrationState] = useState(RegistrationStates.DATA_FORM);

    const validPassword = password => {
        return password.length >= 6;
    };

    const registerUser = async event => {
        event.preventDefault();

        const data = {
            username: document.getElementById("field-user").value,
            name: document.getElementById("field-name").value,
            email: document.getElementById("field-email").value,
            password: document.getElementById("field-pass").value,
        };
        setValues(data);

        // Validate fields
        if (!validPassword(data.password)) {
            setErr({password: "Password must have at least 6 characters"});
        } else {
            setErr({});
            // Submit registration request to the auth server
            try {
                await bridge.register(data);

                // successful registration
                setRegistrationState(RegistrationStates.REGISTERED);
            } catch (err) {
                const response = err.response.data;
                if (response.errorCode === 0) { // invalid field
                    const param = response.field;
                    setErr({[param]: response.description});
                } else {
                    setErr({password: response.description});
                }
            }
        }
    };

    const RegisterForm = () => (
        <>
            <h2>Register</h2>
            < Text variant={'smallPlus'} block>
                Create an account to be able to use the platform
            </Text>

            <form onSubmit={registerUser}>
                <Stack tokens={{childrenGap: 20}}>
                    <TextField id="field-user" label="Username:" autoFocus
                               underlined required defaultValue={values.username}
                               autoComplete="username" errorMessage={err.username}/>
                    <TextField id="field-name" label="Name:"
                               underlined required defaultValue={values.name}
                               autoComplete="name" errorMessage={err.name}/>
                    <TextField id="field-email" label="Email:"
                               underlined required defaultValue={values.email}
                               autocomplete="email" errorMessage={err.email}/>
                    <TextField id="field-pass" label="Password:" type="password"
                               underlined required defaultValue={values.password}
                               autoComplete="current-password" errorMessage={err.password}/>

                    <Link className={style.smallLink} to="/login" style={{color: palette.themePrimary}}>
                        Already have an account? Log In
                    </Link>

                    <Stack horizontal horizontalAlign="end" tokens={{childrenGap: 20}}>
                        <DefaultButton text="Back" allowDisabledFocus/>
                        <PrimaryButton text="Register" type={"submit"} allowDisabledFocus/>
                    </Stack>
                </Stack>
            </form>
        </>
    );

    return (
        <div className={style.flexContainer}>
            <div className={style.box} style={{boxShadow: Depths.depth4}}>
                <Stack className="slideLeft" tokens={{childrenGap: 20}}>
                    {
                        registrationState === RegistrationStates.REGISTERED
                            ? <RegisteredInfoBox/>
                            : <RegisterForm/>
                    }
                </Stack>
            </div>
        </div>
    );
}