import React from 'react';
import {BrowserRouter, Switch, Route, Redirect} from 'react-router-dom';
import {createTheme, loadTheme} from "office-ui-fabric-react";
import {RegisterBox} from "./views/register";
import {useCookies} from 'react-cookie';
import Navbar from "./views/navbar";
import Doctors from "./views/doctors";
import Footer from "./views/footer";
import LoginPanel from "./views/login";
import NotFound from "./views/not-found";
import Home from "./views/home";
import MedicalRecords from "./views/medical-records";
import './App.css';
import {UserRoles} from "./constants";

const customTheme = createTheme({
    palette: {
        themePrimary: '#780b1c',
        themeLighterAlt: '#faf1f2',
        themeLighter: '#e9c7cc',
        themeLight: '#d69ca5',
        themeTertiary: '#ae4f5d',
        themeSecondary: '#881b2b',
        themeDarkAlt: '#6c0a18',
        themeDark: '#5b0815',
        themeDarker: '#43060f',
        neutralLighterAlt: '#faf9f8',
        neutralLighter: '#f3f2f1',
        neutralLight: '#edebe9',
        neutralQuaternaryAlt: '#e1dfdd',
        neutralQuaternary: '#d0d0d0',
        neutralTertiaryAlt: '#c8c6c4',
        neutralTertiary: '#a19f9d',
        neutralSecondary: '#605e5c',
        neutralPrimaryAlt: '#3b3a39',
        neutralPrimary: '#323130',
        neutralDark: '#201f1e',
        black: '#000000',
        white: '#ffffff',
    }
});
loadTheme(customTheme);

const ProtectedRoute = ({children, allowedRoles, ...rest}) => {
    const [cookies] = useCookies(['user_profile']);
    const userProfile = cookies['user_profile'];

    return (
        <Route {...rest}
               render={({location}) =>
                   userProfile && allowedRoles.includes(userProfile.role_id)
                       ? (children)
                       : (<Redirect
                           to={{
                               pathname: "/not-found",
                               state: {from: location}
                           }}
                       />)
               }
        />
    );
};

export default () => {
    const [cookies] = useCookies(['user_profile']);

    // get session information
    const userProfile = cookies['user_profile'];

    return (
        <BrowserRouter basename="/">
            <Navbar user={userProfile}/>

            <Switch>
                <Route path="/not-found" component={NotFound}/>
                <Route path="/login" component={LoginPanel}/>
                <Route path="/register" component={RegisterBox}/>
                <ProtectedRoute path="/doctors"
                                allowedRoles={[UserRoles.NORMAL_USER, UserRoles.DOCTOR, UserRoles.ADMIN]}>
                    <Doctors user={userProfile}/>
                </ProtectedRoute>
                <ProtectedRoute path="/medical-records"
                                allowedRoles={[UserRoles.NORMAL_USER, UserRoles.DOCTOR, UserRoles.ADMIN]}>
                    <MedicalRecords user={userProfile}/>
                </ProtectedRoute>
                <Home/>
            </Switch>

            <Footer/>
        </BrowserRouter>
    );
};
