export const UserRoles = Object.freeze({
    NONE: null,
    ADMIN: 0,
    DOCTOR: 1,
    NORMAL_USER: 2,
});

export const constants = {
    appName: "Medical.Net",
    mail: "md.medical.net@gmail.com",
    phone: "+333 0123456789"
};